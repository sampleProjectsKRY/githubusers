package pl.priv.rynek.client;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
class UserDto {
    private String login;
    private Long id;
    private String name;
    private String type;
    private String avatar_url;
    private LocalDateTime created_at;
    private Long followers;
    private Long public_repos;
    private String followers_url;
    private String repos_url;
}

