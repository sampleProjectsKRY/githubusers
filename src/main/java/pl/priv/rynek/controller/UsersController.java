package pl.priv.rynek.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import pl.priv.rynek.model.User;
import pl.priv.rynek.service.ApiService;

import java.util.NoSuchElementException;

@Slf4j
@RestController
public class UsersController {
    private final ApiService apiService;

    public UsersController(ApiService apiService) {
        this.apiService = apiService;
    }

    @GetMapping("/users/{login}")
    public ResponseEntity<User> getUserInformation(@PathVariable String login) {
        try {
            log.info("New request for user info, login: {}",login);
            apiService.increaseRequestCount(login);
            return new ResponseEntity<>(apiService.getUserInformation(login), HttpStatus.ACCEPTED);
        } catch (HttpClientErrorException.NotFound notFoundException) {
            log.error("Unable to find user with login: {}", login);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (ResourceAccessException resourceAccessException){
            log.error("Unable to connect GitHub API");
            return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
        }
    }

}
