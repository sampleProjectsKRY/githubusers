package pl.priv.rynek.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestCountRepository extends JpaRepository<RequestCount, String> {
}
